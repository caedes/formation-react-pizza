# Formation React Pizza

## Getting Started

Installer toutes les dépendances avec `yarn`

## Run Server

```shell
yarn start
```

## Exercices

### 1. Allez dans `App.js` et importez le fichier `samplePizzas.js`

### 2. Allez dans `Header.js` et suivvez les instructions

### 3. Puis dans `Section.js` et suivvez les instructions

### 4. Allez dans `App.js` et suivez les intruction par points jsuqu'au point 4
