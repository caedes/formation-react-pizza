import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './styles.css';
import HomePage from '../HomePage';

export default () => (
  <BrowserRouter>
    <Route exact path="/" component={HomePage} />
  </BrowserRouter>
);
